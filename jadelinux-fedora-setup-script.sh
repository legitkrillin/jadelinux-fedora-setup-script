#!/bin/bash
# Ultimate fedora set-up script "Weaponize Fedora"
# Created by James Healy for Jadelinux.net
# This is an automation script built to set-up Fedora to my way of working, 
# Please feel free to modify to suit your needs! 

# ----------------------------------
# variables
# ----------------------------------
EDITOR=nano
PASSWD=/etc/passwd
RED='\033[0;41;30m'
STD='\033[0;0;39m'

# ----------------------------------
# User defined function
# ----------------------------------
pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}

# Weaponize Fedora!
one(){

# Remove Horizontal & Vertical scrolls from windows Gnome3 (Remove overlay scroll indicators)
echo "undershoot { background-image: none; }" >> ~/.config/gtk-3.0/gtk.css

# Powerline config for local user! 
echo "if [ -f `which powerline-daemon` ]; then" >> ~/.bashrc
echo "  powerline-daemon -q" >> ~/.bashrc
echo "  POWERLINE_BASH_CONTINUATION=1" >> ~/.bashrc
echo "  POWERLINE_BASH_SELECT=1" >> ~/.bashrc
echo "  . /usr/share/powerline/bash/powerline.sh" >> ~/.bashrc
echo "fi" >> ~/.bashrc

# switching to root for installing repo's and software! 
if [ "$(id -u)" != "0" ]; then
  exec sudo "$0" "$@"
fi

# output logged in user
echo "You are now ${USER}!"
sleep 2

# remove unwanted software
echo Uninstalling unwonted software!
dnf remove -y gnome-boxes gnome-maps orca simple-scan cheese

# install RPMFusion Repo
echo Installing RPMFusion Repo!
su -c 'dnf install -y http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm'
echo Updating Software Cache!
dnf update -y

# install most used software
echo Installing must have software!
dnf install -y thunderbird hexchat vlc gpodder audacity calibre nano gnome-tweak-tool android-tools liferea Zim gparted copyq p7zip p7zip-plugins unrar neofetch telegram-desktop gnome-extensions-app keepassxc quodlibet qbittorrent htop remmina

# install missing codecs
echo Installing Missing Codecs! 
dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel lame\* --exclude=lame-devel ffmpeg gstreamer-ffmpeg

# install Powerline
echo Installing Powerline!
dnf install -y powerline powerline-fonts

echo "if [ -f `which powerline-daemon` ]; then" >> ~/.bashrc
echo "  powerline-daemon -q" >> ~/.bashrc
echo "  POWERLINE_BASH_CONTINUATION=1" >> ~/.bashrc
echo "  POWERLINE_BASH_SELECT=1" >> ~/.bashrc
echo "  . /usr/share/powerline/bash/powerline.sh" >> ~/.bashrc
echo "fi" >> ~/.bashrc

# install dev tools
echo Installing Develipment Tools!
dnf install -y gcc gcc-c++ codeblocks codeblocks-contrib bluefish geany glade

# install power management
echo Installing Power management!
dnf install -y tlp tlp-rdw
systemctl enable tlp

# themes
echo Installing Shell and Icon themes!
dnf install -y arc-theme papirus-icon-theme

# Enabling TRIM 
echo Enabling TRIM!
systemctl enable fstrim.timer 

# jadelinux script info

	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	echo "        JADELINUX ULTIMATE FEDORA SCRIPT"
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	echo "Jadelinux ultimate fedora script is to help you"
	echo "set up and configure fedora quickly and easily,"
	echo
	echo "I created this script to set-up new installs of "
	echo "Fedora to my way fast, please use and modify as"
	echo "you see fit!"
	echo "Any bug’s please report it to info@jadelinux.net"
	echo
	pause
}

# Install Virtualbox
two(){

# ensure running as root
if [ "$(id -u)" != "0" ]; then
  exec sudo "$0" "$@"
fi

echo Installing VirtualBox! 
wget -P /etc/yum.repos.d/ http://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo
dnf update -y
dnf install -y binutils gcc make patch libgomp glibc-headers glibc-devel kernel-headers kernel-devel dkms
dnf install -y VirtualBox-6.1
/usr/lib/virtualbox/vboxdrv.sh setup
usermod -a -G vboxusers $USER
pause
}

# function to display menus
show_menus() {
	clear
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	echo " JADELINUX ULTIMATE FEDORA SETUP"
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	echo " 1. Weaponize Fedora"
	echo " 2. Install VirtualBox"
	echo " 3. Exit"
}

# Exit when user the user select 3 form the menu option.
read_options(){
	local choice
	read -p " Enter choice: " choice
	case $choice in
		1) one ;;
		2) two ;;
		3) exit 0;;
		*) echo -e "${RED}I'm sorry $USER, I can't let you do that!${STD}" && sleep 3
	esac
}

# ----------------------------------------------
# Trap CTRL+C, CTRL+Z and quit singles
# ----------------------------------------------
trap '' SIGINT SIGQUIT SIGTSTP

# ------------------------------------
# Main logic - infinite loop
# ------------------------------------
while true
do

	show_menus
	read_options
done
