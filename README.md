A bash script to setup Fedora fast! It is install or uninstall and configure the following:

Software removed:
gnome-boxes 
gnome-maps 
orca 
simple-scan 
cheese

Software install:
thunderbird 
hexchat 
vlc 
gpodder 
audacity 
calibre 
nano 
gnome-tweak-tool 
android-tools 
liferea 
Zim 
gparted 
copyq 
p7zip p7zip-plugins 
unrar 
neofetch 
telegram-desktop 
gnome-extensions-app 
keepassxc 
quodlibet 
qbittorrent 
htop 
remmina

Missing Codecs:
gstreamer 
lame

Terminal tools:
Powerline

Develipment Tools:
gcc gcc-c++ 
codeblocks codeblocks-contrib 
bluefish 
geany 
glade

Power management:
tlp

Themeing:
arc-theme papirus-icon-theme

Enable TRIM! 
